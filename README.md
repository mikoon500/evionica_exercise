
# evionica_exercise  
  
## How to use the project:
**required**
- Java 11
- Maven 3.6.x
- weight-balancer app running on localhost:8080

to run tests, execute 'mvn clean integration-test' command in terminal.
  
## 1. Test case scenario for DEV team internal use  

ID 1.1  
Title: User searches for non-existent aircraft uuid  
  
Environment: localhost  
Version: 1.0.0-SNAPSHOT  
Preconditions: -  
  
Steps to reproduce:  
  
1. Get all aircraft data using GET /aircraft/all endpoint.  
2. Pick uuid that is not listed, e.g. 'xyz'
3. Send GET /aircraft/xyz request.
  
Expected behaviour : request's response status code is 400, and the "message" field value is "aircraft not found".  
  
Author: Maciej Mika  
  
## 3. Briefly explain your choices of scenarios for points 1 and 2  
  
**Scenario 1:**  
I chose it, because:  
- it's a good example of small, not complicated test, verifying  
one specific app behaviour  
- I feel like many people ignore or forget about negative tests  
and exceptions verification  
- I wanted to automate weight-balance creation, so for this one I needed  
another part of the app ;)  
  
**Scenario 2:**  
- I chose it, because it covers positive case of weight balance creation - which I  
believe is the essence of this app.  
  
## 4. Briefly present and explain your choices regarding technology  
 Technologies I chose are the ones I'm experienced in. I tried not to  
use RC versions or versions I haven't used before - it's always better  
to use older, known to us version than new one which can cause  
unexpected problems.  
  
- Rest-assured - It's probably the most popular REST API testing framework.  
- Junit 5 - it didn't have high importance here (vs Junit 4), because  
I haven't used it's features a lot, but I always try to use Junit 5 in new test suites as it's on the market for some time now and Junit 4 might be deprecated in some point of time - and refactoring from 4 to 5 is a pain, I've already  
experienced it.    
- Hamcrest - because it has more human-readable assertions.  
- Jackson - needed for JSON mapping.  
- Lombok - to reduce boilerplate when it comes to object handling.  
  
**4.1 Very brief explanation of some choices regarding the project**
- project structure reflects app endpoints structure. Thanks to that  
it's very easy to find tests/helpers classes we are interested in.  
- I created helpers, value-object classes to reduce maintenance cost.  
It may seem excessive but when the tests amount rises, it will  
profit with less code repetition and cleaner code. That approach  
was fruitful for me in my previous projects.  
- in real project I would connect to DB  
when getting data/verifying action results instead of using requests.  
I would also create BeforeAll method that would clean all  
weight-balance entries.  
  
  
## 6. Provide short feedback to the task  
  
Exercise description is pretty clear and understandable. It's well divided into  
sections. App is not complex, but it took me some time to fully understand and  
be familiar with its features - thus I think it's good for interview exercise  
purposes. You can test not only coding, but also candidates cognitive abilities  
in short period of time.  
  
Personally it took me 4 hours to complete it - so a bit longer than  
expected (1-3 hrs), but I tried to polish the project as much as I could do.  
I also think, that it would be pretty hard to do it in less than 1-1,5 hr, so  
expected time should be rather 2-4 hrs instead of 1-3.