package weightBalancer.aircraft;

import io.restassured.response.Response;

import java.util.List;

import static io.restassured.RestAssured.given;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static weightBalancer.Properties.AIRCRAFT_ALL_ENDPOINT;
import static weightBalancer.Properties.LOCALHOST_URL;
import static weightBalancer.ResponseHandler.assertResponseCode;

public class AllHelper {

    public static final String URL = LOCALHOST_URL + AIRCRAFT_ALL_ENDPOINT;


    public static List<AircraftValueObject> getAllAircraft() {

        Response response = given()
                .contentType(APPLICATION_JSON)
                .when()
                .get(URL);

        assertResponseCode(response, 200);

        return response.jsonPath().getList("", AircraftValueObject.class);
    }
}
