package weightBalancer.aircraft;

import lombok.Data;

@Data
public class AircraftValueObject {
    Long id;
    String uuid;
    String name;
    Long operatingEmptyWeight;
    Long maxTotalPayload;
    Long maxTotalFuelLoaded;
    Long maxTakeoffWeight;
}
