package weightBalancer.weightBalance;

import lombok.Data;

@Data
public class WeightBalancePOSTBodyValueObject {

    Long totalFuelLoaded;
    Long takeoffWeight;
    String aircraftUuid;
    Long totalPayload;
}
