package weightBalancer.weightBalance;

import io.restassured.response.Response;

import java.util.List;

import static io.restassured.RestAssured.given;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static weightBalancer.Properties.LOCALHOST_URL;
import static weightBalancer.Properties.WEIGHT_BALANCE_ENDPOINT;
import static weightBalancer.ResponseHandler.assertResponseCode;

public class WeightBalanceGetHelper {

    public static final String URL = LOCALHOST_URL + WEIGHT_BALANCE_ENDPOINT;

    public static List<WeightBalanceEntryValueObject> getWeightBalanceEntries() {

        Response response = given()
                .contentType(APPLICATION_JSON)
                .when()
                .get(URL);

        assertResponseCode(response, 200);

        List<WeightBalanceEntryValueObject> entriesList = response.jsonPath().getList("", WeightBalanceEntryValueObject.class);
        return entriesList;
    }
}
