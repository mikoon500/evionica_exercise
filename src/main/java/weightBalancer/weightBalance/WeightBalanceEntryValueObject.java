package weightBalancer.weightBalance;

import lombok.Data;
import weightBalancer.aircraft.AircraftValueObject;

@Data
public class WeightBalanceEntryValueObject {

    Long id;
    AircraftValueObject aircraft;
    Long totalPayload;
    Long totalFuelLoaded;
    Long takeoffWeight;
}
