package weightBalancer;

public class Properties {

    public static final String LOCALHOST_URL = "http://localhost:8080";
    public static final String WEIGHT_BALANCE_ENDPOINT = "/weight-balance";
    public static final String AIRCRAFT_ALL_ENDPOINT = "/aircraft/all";

}
