package weightBalancer.weightBalance;

import io.restassured.response.Response;
import org.junit.jupiter.api.Test;
import weightBalancer.aircraft.AircraftValueObject;
import weightBalancer.aircraft.AllHelper;

import java.util.List;
import java.util.Random;

import static io.restassured.RestAssured.given;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static weightBalancer.Properties.LOCALHOST_URL;
import static weightBalancer.Properties.WEIGHT_BALANCE_ENDPOINT;
import static weightBalancer.ResponseHandler.assertResponseCode;

public class WeightBalancePostAT {

    public static final String URL = LOCALHOST_URL + WEIGHT_BALANCE_ENDPOINT;

    @Test
    public void createWeightBalanceEntryWithMaxValuesAndCheckExistance() {

        List<AircraftValueObject> aircraftList = AllHelper.getAllAircraft();
        Random random = new Random();
        AircraftValueObject randomAircraft = aircraftList.get(random.nextInt(aircraftList.size()));

        WeightBalancePOSTBodyValueObject body = new WeightBalancePOSTBodyValueObject();
        body.setTakeoffWeight(randomAircraft.getMaxTakeoffWeight());
        body.setAircraftUuid(randomAircraft.getUuid());
        body.setTotalFuelLoaded(randomAircraft.getMaxTotalFuelLoaded());
        body.setTotalPayload(randomAircraft.getMaxTotalPayload());

        Response response = given()
                .contentType(APPLICATION_JSON)
                .body(body)
                .when()
                .post(URL);

        assertResponseCode(response, 200);

        WeightBalanceEntryValueObject createdWeightBalanceEntry = response.getBody().as(WeightBalanceEntryValueObject.class);
        List<WeightBalanceEntryValueObject> entriesList = WeightBalanceGetHelper.getWeightBalanceEntries();

        assertThat(entriesList, hasItem(createdWeightBalanceEntry));
    }
}
